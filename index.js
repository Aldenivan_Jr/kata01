function snapCrackle(maxValue){

    let contagem = [];
    let imparPorCinco = ' SnapCrackle, ';
    let impar = ' Snap, ';
    let porCinco = ' Crackle ,';
    
    for( let i = 1; i <= maxValue; i++ ){
             
        if( i % 2 !== 0 ){
            if( i % 5 === 0 ){
                contagem += imparPorCinco;
            } else {
                contagem += impar;
            }
        } else {
            if( i % 5 === 0 ){
                contagem += porCinco;
            } else {
                contagem += i + " ,";
            }
        }
    }   
    return contagem;
}

console.log(snapCrackle());

function prime(num){

    for(let i = 2; i < num; i++){
        if(num % i === 0){
            return false;
        }    
    }
    return true;
}

function snapCracklePrime(maxValue){

    let contagem = [];
    let primo;
    let imparPorCinco = ' SnapCrackle, ';
    let impar = ' Snap, ';
    let porCinco = ' Crackle ,';
    let imparPorCincoPrimo = ' SnapCracklePrime ,'
    let imparPrimo = ' SnapPrime ,'
    let primos = ' Prime ,'

    for(let i = 1; i <= maxValue; i++){
        
        primo = prime(i);

        if(i % 2 !== 0){
            

            if(primo === true){
                if(i % 5 === 0){
                    contagem += imparPorCincoPrimo;
                } if(i === 1){
                    contagem += impar;
                } else if(i !== 5){
                    contagem += imparPrimo;
                }
            }    
            
            if(i % 5 === 0 && !primo === true){
                contagem += imparPorCinco;
            } else if(!primo === true){
                contagem += impar;
            }
        } else {
            
            if(primo === true){
                if(i === 2){
                    contagem += primos;
                }
            }
            
            if(i % 5 === 0){
                contagem += porCinco;
            } else if(!primo === true){
                contagem += i + " ,";
            }
        }
    }   
    return contagem;
}

console.log(snapCracklePrime());